﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ball.Controllers {
	[RequireComponent(typeof(Rigidbody2D))]
	public class BallController : MonoBehaviour {
		private static BallController Instance;

		[Tooltip("Moving force from click")]
		public float Force = 1f;

		private Rigidbody2D _rigidbody;
		private Transform   _transform;
		private Vector2 	_startPos;
		private Camera      _cam;


		private void Start() {
			Instance = this;
			_rigidbody = GetComponent<Rigidbody2D>();
			_cam       = Camera.main;
			_transform = transform;
			_startPos = _transform.position;
		}


		private void Update() {
			CheckInput();
		}


		/// <summary>
		/// Checking input from device
		/// </summary>
		private void CheckInput() {
#if UNITY_STANDALONE
			if (Input.GetMouseButton(0)) {
				MoveTo(Input.mousePosition);
			}
#elif UNITY_ANDROID || UNITY_IOS
            if (Input.touchCount > 0)
            {
              MoveTo(Input.GetTouch(0).position);
            }
#endif
		}


		private void OnCollisionEnter2D(Collision2D other) {
			GameManager.IncrementJumpCounter();
		}


		/// <summary>
		/// Move the ball to the screen position
		/// </summary>
		private void MoveTo(Vector2 screenPosition) {
			Vector3 worldPos = _cam.ScreenToWorldPoint(screenPosition);
			Vector3 position = _transform.position;
			Vector3 offset   = worldPos - position;
//            offset.y = 0;

			_rigidbody.AddForce(offset * Force);
		}


		public static void Restart() {
			Instance._transform.position = Instance._startPos;
			Instance._transform.rotation = Quaternion.identity;
			Instance._rigidbody.velocity = Vector2.zero;
			Instance._rigidbody.angularDrag = 0f;
		}
	}
}