﻿using System.Collections;
using System.Collections.Generic;
using Ball.Data.Game;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace Ball.Controllers {
    public class GameManager : MonoBehaviour {
#region Fields

        public static GameManager Instance;
        public static bool        IsGame;

        private static int                                     JumpCounter;
        private static TweenerCore<float, float, FloatOptions> _timeScale;

        public Text JumpCounterText;

        [SerializeField] private PlanetData EarthData, MoonData, JupiterData;

#endregion


#region Mono Methods
        private void Awake() {
            if (Instance) {
                Destroy(gameObject);
                return;
            }
            Instance = this;

            Time.timeScale = 0;
        }


        private void Update() {
            if (!IsGame) return;
            CheckInput();
        }



        private void CheckInput() {
            if (Input.GetKeyUp(KeyCode.Escape)) {
                StopGame();
                MenuController.FadeOutScreenCover();
                MenuController.ShowMenu();
            }
            else if (Input.GetKeyUp(KeyCode.R)) {
                BallController.Restart();
            }
        }
#endregion


#region Static Methods

        /// <summary>
        /// Increase jump counter and update text
        /// </summary>
        public static void IncrementJumpCounter() {
            Instance.JumpCounterText.text = (++JumpCounter).ToString();
        }


        /// <summary>
        /// Reset jump counter and update text
        /// </summary>
        public static void ResetJumpCounter() {
            JumpCounter = 0;
            Instance.JumpCounterText.text = "0";
        }


        public static void LoadLevel(Planet planet) {
            PlanetData pData;
            switch (planet) {
                case Planet.Moon:    pData = Instance.MoonData;    break;
                case Planet.Jupiter: pData = Instance.JupiterData; break;
                default:             pData = Instance.EarthData;   break;
            }

            // Set planet params
            Physics2D.gravity = new Vector2(0, -pData.Gravity);
            if (Camera.main != null) Camera.main.backgroundColor = pData.SkyColor;

            // Landing on the planet!
            StartGame();
            BallController.Restart();
        }


        private static void StartGame() {
            // Set time scale to 1
            _timeScale?.Kill();
            _timeScale = DOTween.To(()=> Time.timeScale, x=> Time.timeScale = x, 1f, 1f).SetEase(Ease.InQuad).SetUpdate(true);
            IsGame = true;
        }


        private static void StopGame() {
            // Set time scale to 0
            _timeScale?.Kill();
            _timeScale = DOTween.To(()=> Time.timeScale, x=> Time.timeScale = x, 0f, 1f).SetEase(Ease.InQuad).SetUpdate(true);
            IsGame = false;
        }

#endregion

        public enum Planet {
            Earth,
            Moon,
            Jupiter
        }
    }
}