﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Ball.Controllers {
    public class MenuController : MonoBehaviour {
        public static MenuController Instance;

        public Image ScreenCover;
        public GameObject MainMenu;


        private void Awake() {
            if (Instance) {
                Destroy(gameObject);
                return;
            }

            Instance = this;
        }


        public void SelectLevel(int planetNum) {
            GameManager.Planet planet = (GameManager.Planet) planetNum;
            GameManager.LoadLevel(planet);
            FadeInScreenCover();
            HideMenu();
        }


        public void ExitGame() {
            Application.Quit();
        }


        public static void FadeInScreenCover() {
            Instance.ScreenCover.DOFade(0, 1f).SetUpdate(true);
        }


        public static void FadeOutScreenCover() {
            Instance.ScreenCover.DOFade(1, 1f).SetUpdate(true);
        }


        public static void ShowMenu() {
            Instance.MainMenu.SetActive(true);
        }


        public static void HideMenu() {
            Instance.MainMenu.SetActive(false);
        }
    }
}