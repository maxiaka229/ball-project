﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ball.Game.Level {
	[RequireComponent(typeof(SpriteRenderer))]
	public class Platform : MonoBehaviour {
		private SpriteRenderer _sr;


		private void Start() {
			_sr = GetComponent<SpriteRenderer>();
		}


		private void OnCollisionEnter2D(Collision2D other) {
			if (!other.gameObject.CompareTag("Player")) return;
			ChangeColor();
		}


		/// <summary>
		/// Sets random color
		/// </summary>
		private void ChangeColor() {
			float r = Random.value;
			float g = Random.value;
			float b = Random.value;
			_sr.color = new Color(r, g, b);
		}
	}
}