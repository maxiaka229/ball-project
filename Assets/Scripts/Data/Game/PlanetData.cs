using UnityEngine;
using UnityEngine.Serialization;

namespace Ball.Data.Game {
	[CreateAssetMenu(fileName = "New PlanetData", menuName = "Planet Data", order = 51)]
	public class PlanetData : ScriptableObject {
		[SerializeField] private float gravity;
		[SerializeField] private Color skyColor;

		public float Gravity  => gravity;
		public Color SkyColor => skyColor;
	}
}